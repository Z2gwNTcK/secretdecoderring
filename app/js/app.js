class SecretDecoderRing {
    constructor (options) {
        this.iv,
        this.storedKey;
        this.navTabs = document.querySelectorAll('#sdr-container .sdr-nav-tab');
    
        if(localStorage.getItem('sdrKey') !== null) {this.importKey();}

        this.setDefaultTab();
        this.addListeners();
    }

    setDefaultTab() {
        let firstTab = this.navTabs[0];
        firstTab.setAttribute('class', `${firstTab.className} active`);
    };

    addListeners() {
        let tab,
            encryptButton = document.getElementById('sdr-encrypt-button'),
            decryptButton = document.getElementById('sdr-decrypt-button'),
            generateButton = document.getElementById('sdr-generate-button'),
            importButton = document.getElementById('sdr-import-button');

        for(tab of this.navTabs) {
            tab.addEventListener('click', this.handleNavTabClick.bind(this));
        }

        encryptButton.addEventListener('click', this.handleEncryptClick.bind(this));
        decryptButton.addEventListener('click', this.handleDecryptClick.bind(this));
        generateButton.addEventListener('click', this.createKey.bind(this));
        importButton.addEventListener('click', this.importKey.bind(this));
    };

    handleNavTabClick(e) {
        let tab;
        
        if(e.target.className.indexOf('active') == -1) {
            for(tab of this.navTabs) {
                this.togglePane(tab, e.target.dataset.type);
            }
        }
    }

    togglePane(tabElement, clickedTabType) {
        let associatedPaneId = `sdr-${tabElement.dataset.type}-pane`,
            classList = tabElement.className.split(' ');

        if(classList[1] == undefined && tabElement.dataset.type === clickedTabType) {
            tabElement.setAttribute('class', `${classList[0]} active`);
            document.getElementById(associatedPaneId).setAttribute('class','pane');
        }
        else {
            tabElement.setAttribute('class', `${classList[0]}`);
            document.getElementById(associatedPaneId).setAttribute('class','pane hidden');
        }
    }

    toggleLed() {
        let ledElement = document.querySelector('#sdr-container nav section .led'),
            ledClassArray = ledElement.classList;

        if (ledClassArray[1] !== "green") {
            ledElement.setAttribute('class','led green');
        }
    };

    createKey() {
        window.crypto.subtle.generateKey({name: "AES-GCM", length: 256}, true, ["encrypt", "decrypt"]).then((key) => {
            this.storedKey = key;
            this.iv = window.crypto.getRandomValues(new Uint8Array(12));
            this.toggleLed();
            this.exportKey(key);
        });
    }

    exportKey(key) {
        let textAreaElement = document.querySelector('#sdr-generate-pane textarea');

        window.crypto.subtle.exportKey("jwk", key).then((rawKey) => {
            rawKey.iv = btoa(this.iv);
            textAreaElement.value = JSON.stringify(rawKey);
            localStorage.setItem('sdrKey', JSON.stringify(rawKey));
        });
    }

    importKey() {
        let importTextareaElement = document.querySelector('#sdr-import-pane textarea'),
            locallyStoredKey = localStorage.getItem('sdrKey'),
            serializedJWK = (locallyStoredKey !== null) ? locallyStoredKey : importTextareaElement.value,
            rawJWK = JSON.parse(serializedJWK);

        if(locallyStoredKey === null) {localStorage.setItem('sdrKey', serializedJWK);}
        this.iv = this.toBufferArray(atob(rawJWK.iv).split(","));

        window.crypto.subtle.importKey("jwk", rawJWK, {name: "AES-GCM"}, true, ["encrypt", "decrypt"]).then((key)=> {
            this.storedKey = key;
            importTextareaElement.value = '';
            this.toggleLed();
        });
    };

    toBufferArray(str) {
        let i,
            strLength = str.length,
            buf = new ArrayBuffer(str.length),
            bufView = new Uint8Array(buf);

        for (i = 0; i < strLength; i++) {
            bufView[i] = (typeof str === "string") ? str.charCodeAt(i) : str[i];
        }

        return buf;
    }

    handleEncryptClick(e) {
        let plainText = document.querySelector('#sdr-encrypt-pane textarea.sdr-plaintext').value,
            encryptedResultElement = document.querySelector('#sdr-encrypt-pane textarea.sdr-encrypted'),
            encoder = new TextEncoder();

        window.crypto.subtle.encrypt({name: 'AES-GCM',iv: this.iv},this.storedKey, encoder.encode(plainText))
        .then(function(encrypted){
            let arrayBufferToString = String.fromCharCode.apply(null, new Uint8Array(encrypted));
            encryptedResultElement.value = btoa(arrayBufferToString);
        })
        .catch(function(err){
            console.error(err);
        });
    };

    handleDecryptClick(e) {
        let deserializedString = atob(document.querySelector('#sdr-decrypt-pane textarea.sdr-encrypted').value),
            bufferArray = this.toBufferArray(deserializedString);

        window.crypto.subtle.decrypt({name: 'AES-GCM',iv: this.iv},this.storedKey,bufferArray)
        .then(function(decrypted){
            let decoder = new TextDecoder();
            document.querySelector('#sdr-decrypt-pane textarea.sdr-plaintext').value = decoder.decode(decrypted);
        })
        .catch(function(err){
            console.dir(err);
        });
    };

    debug(msg) {
        let bgPage = chrome.extension.getBackgroundPage();

        if(bgPage.console && bgPage.console.log) {
            bgPage.console.log(msg);
        }
    }
}

document.addEventListener('DOMContentLoaded',() => {
    let sdr = new SecretDecoderRing({"debug":false});
});