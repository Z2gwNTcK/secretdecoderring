# Secret Decoder Ring

Secret Decoder Ring is a Google Chrome extension which harnesses the power of the [Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API) to give users the ability to encrypt and decrypt messages using  generated key.

![Secret Decoder Ring Highlights](screenshots/highlights.png)

# Installation
Simply download the latest release zip file, and do the following:

1. In a Chrome browser window, open `chrome://extensions`.
2. In the upper-right corner, turn on Developer Mode.
3. Click the "Load Unpacked" button.
4. Select the release you just downloaded.

# Usage
There are four main features of the Secret Decoder Ring extension which are discussed below.

## Encrypt
_NOTE: To use this feature, you must already have a cryptographic key loaded. You can do this by either generating a key or importing a key that has been given to you._
1. Type your message in the top textarea and click the "ENCRYPT" button.
2. Copy the encrypted string and send it onwards!

## Decrypt
_NOTE: To use this feature, you must already have a cryptographic key loaded. You can do this by either generating a key or importing a key that has been given to you._
1. Paste your encrypted string in the top textarea and click the "DECRYPT" button.
2. Your decrypted message will appear in the bottom textarea.

## Generate
_NOTE: Every time you click the "GENERATE" button, a new key will be generated and stored, rendering previous keys useless._
1. Click the "GENERATE" button to generate a new key. This key must be shared with anyone you wish to pass messages to.
2. You will note that the "key loaded" indicator is now green.

## Import
1. Paste the cryptographic key you received in the textarea and click the "IMPORT" button.
2. You will note that the "key loaded" indicator is now green.